"use strict";

//Password - Toggling eye-icon ON/OFF

const visibilityToggle = document.querySelector('.visibility');

const input = document.querySelector('.password');

var password = true;

visibilityToggle.addEventListener('click', function() {
  if (password) {
    input.setAttribute('type', 'text');
    visibilityToggle.innerHTML = "visibility_off";
  } else {
    input.setAttribute('type', 'password');
    visibilityToggle.innerHTML = "visibility";
  }
  password = !password;
})

//Confirm Password - Toggling eye-icon ON/OFF

const visibilityToggle2 = document.querySelector('.visibility2');

const input2 = document.querySelector('.password2');

var password2 = true;

visibilityToggle2.addEventListener('click', function() {
  if (password2) {
    input2.setAttribute('type', 'text');
    visibilityToggle2.innerHTML = "visibility_off";
  } else {
    input2.setAttribute('type', 'password');
    visibilityToggle2.innerHTML = "visibility";
  }
  password2 = !password2;
})

////////////////////////////////////////////////////////////////
//Showing the form inputs in console

function showInputs() {
  console.log(document.getElementById('username').value); 
  console.log(document.getElementById('email').value);
  console.log(document.getElementById('password').value);
  console.log(document.getElementById('password2').value);
}

///////////////////////////////////////////
//Reseting the form - reloading the page

function reloadPage() {
  document.getElementById('myForm').reset();
}